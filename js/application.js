// количество барабанов
var countDrum = 3;
// размер одного элемента
var symbolHeight = 144;
var symbolWidth = 216;
// количество элементов
var countSymbol = 5;//20;
// замедление
var speedStep = 1;//0.2;
// набор различных символов для барабана
//var arraySymbol =['a','b','c','d','e'];
var arraySymbol =['a','b'];

/**
 * Создание div для барабана
 *
 * @param parent родитель-барабан
 * @param className имя класса с изображением символа
 * @param id идентификатор изображения
 */
function createDivToDrum(parent, className, id){
    var childElement = document.createElement('div');
    childElement.className = className;
    childElement.id = id;
    return parent.appendChild(childElement);
}

/**
 * Инициализация барабана, заполнения случайными символами
 * @param countSymbolOnDrum общее количество символов на барабане
 * @param parentElement родитель-барабан
 * @param numDrum номер барабана
 */
function init(countSymbolOnDrum, parentElement, numDrum){
    var res = [];
    // сколько раз надо сгенерировать случайные наборы из символов
    for (var i = 0; i< countSymbolOnDrum/arraySymbol.length ; i++){
        var r = [];
        // генерация одного набора из уникальных символов
        while (r.length < arraySymbol.length){
            var num = Math.floor(Math.random() * arraySymbol.length);
            if(r.indexOf(arraySymbol[num]) === -1){
                r.push(arraySymbol[num])
            }
        }
        res = res.concat(r);
    }

    //parentElement.innerHTML = '';
    var parent = createDivToDrum(parentElement, 'XXX', 'drum'+numDrum);
    createDivToDrum(parent, 'X '+res[res.length-1], numDrum*100 -1);
    // создание разметки
    for (i=0; i<res.length; i++) {
        createDivToDrum(parent, 'X '+res[i], numDrum*100 + i);
    }
    // добавляем в конец начальный элемент, чтобы визуально барабан не стопорился при смене оборота
    createDivToDrum(parent, 'X '+res[0], numDrum*100 + i);


}

// инициализируем барабаны
for (var i =0 ; i<countDrum; i++){
    init(countSymbol, document.getElementById('drumParent'), i);
}


function LetsGo(t) {
    clearInterval(LetsGo.interval);
    
    var x = [];
    for (var i =0 ; i<countDrum; i++) {
        var ob = 2 + Math.floor(Math.random() * 3), // количество оборотов 2 минимум, 5 - максимум
            num = 1 + Math.floor(Math.random() * countSymbol), // выигравший нумер
            symbolWin = document.getElementById('drum'+i).childNodes[num].className,
            id = document.getElementById('drum'+i).childNodes[num].id,

            o = {
                symbolWin: symbolWin,
                id: id,
                ob: ob,
                num: num,
                a: document.getElementById('drum'+i), // это наш контрол с классом drum
                // время=(speedStart)/speedStep
                // расстояние=(speedStart/2)*время -арифм прогр
                // ускорение=speedStart^2/(2*speedStep)
                // speedStart=sqrt(расстояние*2*speedStep)
                speed: Math.sqrt(((ob * countSymbol + num) * symbolHeight) * speedStep * 2), // скорость
                //scr: - symbolHeight / 4 // смещение, чтобф симолы не пересккивали вверх
                scr: - symbolHeight - symbolHeight / 2 // смещение, чтобф симолы не пересккивали вверх
            };
        x.push(o);
        //console.log(Math.sqrt(((ob * countSymbol + num) * symbolHeight) * speedStep * 2));
        console.log(symbolWin+' '+id+' num=' +num);
    }


    LetsGo.interval = setInterval(
        function () {
            t.disabled =true;
            var i = x.length,
                complete = true;
            while (i--) {
                var a = x[i];
                a.scr += a.speed;
                if (a.speed > 2 * speedStep) {
                    a.speed -= speedStep;
                    complete = false;
                } else if (a.speed > 0) {
                    var t = Math.round(a.scr / symbolHeight);
                    if (t >= countSymbol) t = 0;
                    //console.log(i, t, a.scr - symbolHeight * t, a.ob, a.num);
                    a.scr = symbolHeight * t;
                    a.speed = 0;
                    complete = false;
                }
                if (a.scr > (symbolHeight * countSymbol))
                    a.scr -= symbolHeight * countSymbol;
                a.a.scrollTop = Math.floor(a.scr);
            }

            if (complete) {
                clearInterval(LetsGo.interval);
                t.disabled =false;
                var isWin = true;
                for (var j = 1; j<countDrum; j++) {
                    if (x[j].symbolWin !== x[0].symbolWin){
                        isWin = false;
                        return;
                    }
                }
                if (isWin){
                    for (j = 0; j<countDrum; j++) {
                        animate(x[j].id)
                    }
                }
            }
        }, 20)
}

function animate(id) {
    var elem = document.getElementById(id);
    var pos = 0;
    var idInterval = setInterval(frame, 100);
    function frame() {
        if (pos <= -symbolWidth * 4) {
            clearInterval(idInterval);
            elem.style.backgroundPosition = '0px 0px';
        } else {
            pos -= symbolWidth;
            elem.style.backgroundPosition = pos+'px 0px';
        }
    }
}